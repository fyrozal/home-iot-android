package com.example.android.homeiot_fan;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.LinearInterpolator;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "MainActivity";

    ToggleButton toggleButton;
    ImageView imageView;
    SeekBar seekBar;
    Switch switchView;

    ObjectAnimator rotateAnimator;

    GradientDrawable gd;
    final int FAN_SPEED[] = {0, 5000, 3000, 1000};
    boolean isOn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gd = new GradientDrawable();

        toggleButton = findViewById(R.id.btnToggle);
        imageView = findViewById(R.id.imageView);
        seekBar = findViewById(R.id.seekBar);
        switchView = findViewById(R.id.switchView);


        rotateAnimator = ObjectAnimator.ofFloat(imageView, "rotation", 0, 360);
        rotateAnimator.setRepeatCount(ValueAnimator.INFINITE);
        rotateAnimator.setInterpolator(new LinearInterpolator());
        rotateAnimator.setDuration(1000);

        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setGradientType(GradientDrawable.RADIAL_GRADIENT);
        gd.setGradientRadius(330);

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d(TAG, "Toogle is " + isChecked);
                if (isChecked && isOn ==false) {
//                    rotateFan();
                    seekBar.setPressed(true);
                } else {
                    if(isOn ==true)
                        seekBar.setPressed(false);
                }
                isOn = isChecked;


            }
        });

        switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d(TAG, "Switch is " + isChecked);
                if (isChecked) {
                    turnLightOn();
                } else {
                    turnLightOff();
                }
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d(TAG, "Seekbar : " + progress);
    rotateFan(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }


    private void rotateFan(int index) {

        if (index == 0) {
            rotateAnimator.end();
            toggleButton.setChecked(false);
            return;
        }

        if(!isOn){
            toggleButton.setChecked(true);

        }
        rotateAnimator.setDuration(FAN_SPEED[index]);
        rotateAnimator.start();

    }

    private void stopFan() {
        rotateAnimator.end();

    }

    private void turnLightOn() {
        gd.setColors(new int[]{Color.YELLOW, Color.TRANSPARENT});
        imageView.setBackground(gd);

    }

    private void turnLightOff() {
        imageView.setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    protected  void onResume(){

    }

    @Override
    protected void onPause(){

    }
}
